# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.db import models

from utils.models.abstracts import SluggedAbstractModel, ActivatedAbstractModel, TimeStampedAbstractModel


@python_2_unicode_compatible
class ManagerCategory(SluggedAbstractModel, TimeStampedAbstractModel, ActivatedAbstractModel):

    IS_PUBLIC_CHOICES = (
        ('NOT_DEFINED', _(u'non défini')),
        ('IS_PUBLIC', _(u'publique')),
        ('NOT_PUBLIC', _(u'privé')),
    )

    is_public = models.CharField(_(u'est publique ?'), max_length=11, choices=IS_PUBLIC_CHOICES, default='NOT_DEFINED')

    class Meta:
        verbose_name = _(u'catégorie de gestionnaire')
        verbose_name_plural = _(u'catégories des gestionnaires')
        ordering = ('label',)
        unique_together = ('label', 'slug')

    def __str__(self):
        return self.label
