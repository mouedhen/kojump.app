# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.db import models

from utils.models.abstracts import SluggedAbstractModel, ActivatedAbstractModel, TimeStampedAbstractModel
from equipments.models.equipment_category_family import EquipmentCategoryFamily


@python_2_unicode_compatible
class EquipmentCategory(SluggedAbstractModel, TimeStampedAbstractModel, ActivatedAbstractModel):

    family = models.ForeignKey(EquipmentCategoryFamily, on_delete=models.CASCADE, related_name='categories')

    class Meta:
        verbose_name = _(u'catégorie d\'équipements')
        verbose_name_plural = _(u'catégories d\'équipements')
        unique_together = ('label', 'slug',)
        ordering = ('label',)

    def __str__(self):
        return self.label
