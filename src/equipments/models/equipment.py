# -*- coding: utf-8 -*-
import random
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.utils.functional import cached_property
from django.db import models
from django.contrib.contenttypes.fields import GenericRelation

from equipments.models.discipline_sportive import SportDiscipline
from equipments.models.institution import Institution
from equipments.models.site_environment import SiteEnvironment
from equipments.models.site_ground import SiteGround
from utils.models.abstracts import SluggedAbstractModel, DescribedAbstractModel
from publishing.models.postable import Postable
from geolocation.models import GeoLocationAbstarctModel

from equipments.models.equipment_category import EquipmentCategory

from utils.models.attachement import AttachedMedia
from publishing.models.comment import Comment
from publishing.models.rate import Rate
from publishing.models.favorite import Favorite


def generate_color():
    color = '#{:02x}{:02x}{:02x}'.format(*map(lambda x: random.randint(0, 255), range(3)))
    return color


@python_2_unicode_compatible
class Equipment(SluggedAbstractModel, DescribedAbstractModel, Postable,
                GeoLocationAbstarctModel):

    YES_NO_CHOICES = (
        ('yes', _(u'oui')),
        ('no', _(u'non')),
        ('not concerned', _(u'non concerné')),
    )

    # General
    code = models.CharField(_('code'), max_length=12, primary_key=True)
    cover = models.ImageField(
        _('couverture'), blank=True, null=True,
        default='default-institution-thumbnail.png', upload_to='institution/cover',
        help_text=_(u'Cette image est utilisé en tant que couverture pour l\'institution')
    )
    # Address
    # commune = models.ForeignKey(
    #     Commune, on_delete=models.CASCADE, related_name='equipments',
    #     blank=True, verbose_name='commune'
    # )
    # Additional category information
    is_erp_cts = models.BooleanField(_('Chapitau tente ?'), default=False, blank=True)
    is_erp_ref = models.BooleanField(_('Etablissement flottant ?'), default=False, blank=True)
    is_erp_l = models.BooleanField(_('Salle polyvalente ?'), default=False, blank=True)
    is_erp_n = models.BooleanField(_('Restaurant et débit de boisson ?'), default=False, blank=True)
    is_erp_o = models.BooleanField(_('Hôtel ?'), default=False, blank=True)
    is_erp_oa = models.BooleanField(_('Hôtel restaurant d\'altitude ?'), default=False, blank=True)
    is_erp_p = models.BooleanField(_('Salle de danse et jeux ?'), default=False, blank=True)
    is_erp_pa = models.BooleanField(_('Etablissement en plein air ?'), default=False, blank=True)
    is_erp_r = models.BooleanField(_('Enseignement et colo ?'), default=False, blank=True)
    is_erp_rpe = models.BooleanField(_('PE ?'), default=False, blank=True)
    is_erp_sg = models.BooleanField(_('Structure gonflable ?'), default=False, blank=True)
    is_erp_x = models.BooleanField(_('Etablissement sportif couvert ?'), default=False, blank=True)
    # Utils
    is_always_open = models.BooleanField(_('7d/7d - 24h/24h ?'), default=False, blank=True)
    only_season = models.BooleanField(_('ouvert selement dans les saisons ?'), default=False, blank=True)
    # Target
    for_schools_use = models.BooleanField(_('pour utilisation scolaire ?'), default=False, blank=True)
    for_clubs_use = models.BooleanField(_('pour utilisation des clubs ?'), default=False, blank=True)
    for_others_use = models.BooleanField(_('pour utilisation autre ?'), default=False, blank=True)
    for_individual_use = models.BooleanField(_('pour utilisation individuelle ?'), default=False, blank=True)
    # Disabled person accessibility information
    accessible_handicapped_m = models.CharField(_('accéssible aux handicapés à mobilité réduite ?'), max_length=14, default='not concerned', choices=YES_NO_CHOICES)
    accessible_handicapped_s = models.CharField(_('accéssible aux handicapés sensoriaux ?'), max_length=14, default='not concerned', choices=YES_NO_CHOICES)
    # Additional information
    is_public = models.BooleanField(_('est publique ?'), default=False, blank=True)
    price = models.TextField(_('liste des prix'), blank=True, null=True)

    institution = models.ForeignKey(Institution, on_delete=models.CASCADE, related_name='equipments', blank=True, null=True)
    category = models.ForeignKey(EquipmentCategory, on_delete=models.SET_NULL, related_name='equipments', blank=True, null=True)
    disciplines = models.ManyToManyField(SportDiscipline, related_name='equipments', through='EquipmentSportDiscipline')
    ground = models.ForeignKey(SiteGround, on_delete=models.SET_NULL, related_name='equipments', blank=True, null=True)
    environment = models.ForeignKey(SiteEnvironment, on_delete=models.SET_NULL, blank=True, null=True)

    # Social interactions
    medias = GenericRelation(AttachedMedia)
    comments = GenericRelation(Comment)
    rating = GenericRelation(Rate)
    favorites = GenericRelation(Favorite)

    class Meta:
        verbose_name = _('equipement sportif')
        verbose_name_plural = _('equipements sportifs')

    @cached_property
    def pin_color(self):
        discipline = self.disciplines.first()
        if discipline:
            return discipline.pin_color
        return generate_color()

    def __str__(self):
        return '{} - {}'.format(self.label, self.institution.label)

    def image(self):
        return self.cover.url

    def get_absolute_url(self):
        return reverse('equipments:equipment-details', kwargs={'pk': self.pk, 'slug': self.slug})


class EquipmentSportDiscipline(models.Model):
    equipment = models.ForeignKey(Equipment, on_delete=models.CASCADE)
    discipline = models.ForeignKey(SportDiscipline, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('equipement / discipline association')
        verbose_name_plural = _('equipements / disciplines associations')
