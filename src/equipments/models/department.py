# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.db import models

from utils.models.abstracts import SluggedAbstractModel, ActivatedAbstractModel, TimeStampedAbstractModel


@python_2_unicode_compatible
class Department(SluggedAbstractModel, TimeStampedAbstractModel, ActivatedAbstractModel):

    code = models.CharField(_('code département'), max_length=4, primary_key=True)

    class Meta:
        verbose_name = _(u'département')
        verbose_name_plural = _(u'départements')
        ordering = ('label',)
        unique_together = ('label', 'slug')

    def __str__(self):
        return self.label
