# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible

from utils.models.abstracts import SluggedAbstractModel, ActivatedAbstractModel, TimeStampedAbstractModel


@python_2_unicode_compatible
class EquipmentCategoryFamily(SluggedAbstractModel, TimeStampedAbstractModel, ActivatedAbstractModel):

    class Meta:
        verbose_name = _('famille d\'équipements')
        verbose_name_plural = _('famille d\'équipements')
        unique_together = ('label', 'slug',)
        ordering = ('label',)

    def __str__(self):
        return self.label
