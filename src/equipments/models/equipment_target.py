# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible

from utils.models.abstracts import SluggedAbstractModel, ActivatedAbstractModel, TimeStampedAbstractModel


@python_2_unicode_compatible
class EquipmentTarget(SluggedAbstractModel, TimeStampedAbstractModel, ActivatedAbstractModel):

    class Meta:
        verbose_name = _(u'publique cible')
        verbose_name_plural = _(u'publiques cibles')
        ordering = ('label',)
        unique_together = ('label', 'slug')

    def __str__(self):
        return self.label
