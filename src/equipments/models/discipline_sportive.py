# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible

from django.db import models

from utils.models.abstracts import (SluggedAbstractModel, DescribedAbstractModel,
                                    ActivatedAbstractModel, TimeStampedAbstractModel)
from equipments.models.activity import Activity


@python_2_unicode_compatible
class SportDiscipline(SluggedAbstractModel, DescribedAbstractModel, ActivatedAbstractModel, TimeStampedAbstractModel):

    reference = models.PositiveIntegerField(_('reférence'), primary_key=True, default=0)
    activity = models.ForeignKey(
        Activity, on_delete=models.CASCADE, related_name='sports_disciplines', verbose_name=_('activité')
    )

    class Meta:
        verbose_name = _('discipline sportive')
        verbose_name_plural = _('disciplines sportives')
        unique_together = ('label', 'slug',)
        ordering = ('label',)

    @property
    def pin_color(self):
        if self.activity:
            return self.activity.pin_color
        return None

    def __str__(self):
        return self.label
