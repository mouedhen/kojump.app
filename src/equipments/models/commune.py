# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.db import models

from utils.models.abstracts import SluggedAbstractModel, ActivatedAbstractModel, TimeStampedAbstractModel
from equipments.models.department import Department


@python_2_unicode_compatible
class Commune(SluggedAbstractModel, TimeStampedAbstractModel, ActivatedAbstractModel):

    code_insee = models.CharField(_('code INSEE'), max_length=6, primary_key=True)
    department = models.ForeignKey(Department, on_delete=models.CASCADE, related_name='communes', blank=True)

    class Meta:
        verbose_name = _(u'communes')
        verbose_name_plural = _(u'communes')
        ordering = ('label',)

    def __str__(self):
        return self.label
