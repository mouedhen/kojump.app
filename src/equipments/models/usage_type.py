# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible

from utils.models.abstracts import SluggedAbstractModel, ActivatedAbstractModel, TimeStampedAbstractModel


@python_2_unicode_compatible
class UsageType(SluggedAbstractModel, TimeStampedAbstractModel, ActivatedAbstractModel):

    class Meta:
        verbose_name = _(u'type d\'utilisation')
        verbose_name_plural = _(u'types d\'utilisation')
        ordering = ('label',)
        unique_together = ('label', 'slug')

    def __str__(self):
        return self.label
