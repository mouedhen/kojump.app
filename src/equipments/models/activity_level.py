# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible

from utils.models.abstracts import SluggedAbstractModel, ActivatedAbstractModel, TimeStampedAbstractModel


@python_2_unicode_compatible
class ActivityLevel(SluggedAbstractModel, TimeStampedAbstractModel, ActivatedAbstractModel):

    class Meta:
        verbose_name = _(u'niveau de l\'activité')
        verbose_name_plural = _(u'niveaux des activités')
        ordering = ('label',)
        unique_together = ('label', 'slug')

    def __str__(self):
        return self.label
