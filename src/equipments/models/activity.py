# -*- coding: utf-8 -*-
import random
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from django.core.urlresolvers import reverse

from colorfield.fields import ColorField

from utils.models.abstracts import (SluggedAbstractModel, DescribedAbstractModel,
                                    ActivatedAbstractModel, TimeStampedAbstractModel)


def generate_color():
    color = '#{:02x}{:02x}{:02x}'.format(*map(lambda x: random.randint(0, 255), range(3)))
    return color


@python_2_unicode_compatible
class Activity(SluggedAbstractModel, DescribedAbstractModel, ActivatedAbstractModel, TimeStampedAbstractModel):

    class Meta:
        verbose_name = _(u'Activité')
        verbose_name_plural = _(u'Activités')
        unique_together = ('label', 'slug',)
        ordering = ('label',)

    reference = models.IntegerField(_(u'reférence'), primary_key=True)
    image = models.ImageField(_(u'image'), null=True,)
    pin_color = ColorField(_(u'couleur du pin'), default=generate_color())

    def __str__(self):
        return self.label

    def get_absolute_url(self):
        return reverse('equipments:activity-details', kwargs={'pk': self.pk, 'slug': self.slug})
