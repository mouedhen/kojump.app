# -*- coding: utf-8 -*-
from django.conf.urls import url

from .views.activity import ActivityDetailView
from .views.institution import (InstitutionPublicDetailView,
                                FavoriteInstitutions,
                                CommentedInstitutions,
                                EquipmentPublicDetailView,
                                InstitutionPhotoUploadView,
                                EquipmentPhotoUploadView)
from .views.search import SearchView

app_name = 'equipments'

urlpatterns = [

    url(r'activities/(?P<pk>[0-9]+)/(?P<slug>[-\w]+)/$',
        ActivityDetailView.as_view(),
        name='activity-details'),

    url(r'institutions/(?P<pk>[0-9]+)/(?P<slug>[-\w]+)/$', InstitutionPublicDetailView.as_view(),
        name='institution-details'),

    url(r'equipments/(?P<pk>[0-9]+)/(?P<slug>[-\w]+)/$', EquipmentPublicDetailView.as_view(),
        name='equipment-details'),

    url(r'institutions/favorites/$', FavoriteInstitutions.as_view(), name='institutions-favorites'),
    url(r'institutions/commented/$', CommentedInstitutions.as_view(), name='institutions-commented'),

    url(r'search/$', SearchView.as_view(), name='search'),

    url(
        regex=r'^(?P<pk>\d+)/institution-upload/$',
        view=InstitutionPhotoUploadView.as_view(),
        name='institution_photo_upload_view',
    ),

    url(
        regex=r'^(?P<pk>\d+)/equipment-upload/$',
        view=EquipmentPhotoUploadView.as_view(),
        name='equipment_photo_upload_view',
    ),
]
