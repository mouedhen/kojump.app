# -*- coding: utf-8 -*-
from django.views.generic import ListView, DetailView

from equipments.models.activity import Activity
from equipments.forms.activity import MultiSelectActivitiesForm


class ActivityListView(ListView):

    model = Activity
    template_name = 'equipments/partial/activities.html'

    paginate_by = 6

    def get_queryset(self):
        return Activity.objects.filter(is_active=True)

    def get_context_data(self, **kwargs):
        context = super(ActivityListView, self).get_context_data(**kwargs)
        context['form_activities'] = MultiSelectActivitiesForm
        return context


class ActivityDetailView(DetailView):

    model = Activity
    template_name = 'site/activity-detail.html'
