# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-02 10:02
from __future__ import unicode_literals

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('is_active', models.BooleanField(default=True, verbose_name='est activé ?')),
                ('street_address', models.CharField(blank=True, max_length=254, verbose_name='adresse postale')),
                ('street_address2', models.CharField(blank=True, max_length=254, verbose_name='adresse postale (complément)')),
                ('postal_code', models.CharField(blank=True, max_length=10, verbose_name='code postal')),
                ('city', models.CharField(blank=True, max_length=100, verbose_name='ville')),
                ('country', models.CharField(blank=True, max_length=100, verbose_name='pays')),
                ('photo', models.ImageField(blank=True, upload_to='users/%Y/%m/%d', verbose_name='image de profil')),
                ('birth_date', models.DateField(blank=True, null=True, verbose_name='date de naissance')),
                ('gender', models.CharField(choices=[('M', 'homme'), ('F', 'femme'), ('N', 'non spécifié')], default='N', max_length=1, verbose_name='sexe')),
                ('bio', models.TextField(blank=True, max_length=1024, null=True, verbose_name='présentation')),
                ('phone_number', models.CharField(blank=True, max_length=16, null=True, validators=[django.core.validators.RegexValidator(message="Le numéro doit avoir la forme : '+999999999'", regex='^\\+?1?\\d{9,16}$')], verbose_name='téléphone')),
                ('mobile_number', models.CharField(blank=True, max_length=16, null=True, validators=[django.core.validators.RegexValidator(message="Le numéro doit avoir la forme : '+999999999'", regex='^\\+?1?\\d{9,16}$')], verbose_name='mobile')),
                ('fax_number', models.CharField(blank=True, max_length=16, null=True, validators=[django.core.validators.RegexValidator(message="Le numéro doit avoir la forme : '+999999999'", regex='^\\+?1?\\d{9,16}$')], verbose_name='fax')),
                ('email_confirmed', models.BooleanField(default=False, verbose_name='adresse e-mail confirmé ?')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'profil utilisateur',
                'verbose_name_plural': 'profils utilisateurs',
            },
        ),
    ]
