# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.core.validators import RegexValidator
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.exceptions import ValidationError

from django.contrib.auth.models import User

from utils.models.abstracts import ActivatedAbstractModel, LocatedAbstractModel


GENDER_CHOICES = (
    ('M', _(u'homme')),
    ('F', _(u'femme')),
    ('N', _(u'non spécifié')),
)


@python_2_unicode_compatible
class Profile(ActivatedAbstractModel, LocatedAbstractModel):

    # Authentication
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    # General
    photo = models.ImageField(_(u'image de profil'), upload_to='users/%Y/%m/%d', blank=True)
    birth_date = models.DateField(_(u'date de naissance'), blank=True, null=True)
    gender = models.CharField(_(u'sexe'), max_length=1, choices=GENDER_CHOICES, default='N')
    bio = models.TextField(_(u'présentation'), max_length=1024, null=True, blank=True)
    # Contact
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,16}$', message=_('Le numéro doit avoir la forme : \'+999999999\''))
    phone_number = models.CharField(_(u'téléphone'), max_length=16, blank=True, null=True, validators=[phone_regex])
    mobile_number = models.CharField(_(u'mobile'), max_length=16, blank=True, null=True, validators=[phone_regex])
    fax_number = models.CharField(_(u'fax'), max_length=16, blank=True, null=True, validators=[phone_regex])
    # Administration
    email_confirmed = models.BooleanField(_(u'adresse e-mail confirmé ?'), default=False)

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = _(u'profil utilisateur')
        verbose_name_plural = _('profils utilisateurs')


@receiver(post_save, sender=User)
def create_profile_handler(sender, instance, created, **kwargs):
    """
    Handle profile creation on user creation
    """
    if not created:
        return None
    profile = Profile(user=instance)
    profile.save()


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


@receiver(post_save, sender=User)
def unique_email_handler(sender, instance, created, **kwargs):
    """
    Handle email validation (unique)
    """
    email = instance.email
    username = instance.username
    if not email:
        # raise ValidationError('email address required')
        return
    if sender.objects.filter(email=email).exclude(username=username).count() > 0:
        raise ValidationError(_(u'l\'adresse e-mail existe déjà.'))
