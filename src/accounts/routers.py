# -*- coding: utf-8 -*-
from rest_framework import routers

from accounts.services.profile import UserViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
