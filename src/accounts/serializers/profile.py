# -*- coding: utf-8 -*-
from rest_framework import serializers

from django.contrib.auth.models import User

from accounts.models.profile import Profile


class UserProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = Profile
        fields = ('photo', 'birth_date', 'gender', 'bio',
                  'phone_number', 'mobile_number', 'fax_number',
                  'street_address', 'street_address2', 'postal_code', 'city', 'country',
                  'is_active')


class UserSerializer(serializers.ModelSerializer):

    profile = UserProfileSerializer(read_only=False)

    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff', 'is_active', 'profile')
