# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.apps import AppConfig


class AccountsConfig(AppConfig):
    name = 'accounts'
    verbose_name = _(u'compte utilisateur')
    verbose_name_plural = _(u'comptes utilisateurs')
