# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView


app_name = 'legal'

urlpatterns = [
    url(r'^copyright-policy/$',
        cache_page(60*15)(TemplateView.as_view(template_name='legal/copyright-policy.html')),
        name='copyright'),

    url(r'^privacy-policy/$',
        cache_page(60*15)(TemplateView.as_view(template_name='legal/privacy-policy.html')),
        name='privacy'),
]
