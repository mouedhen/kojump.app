# -*- coding: utf-8 -*-
from rest_framework import viewsets

from .serializer import ActivitySerializer
from equipments.models.activity import Activity


class ActivityViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = Activity.objects.all().filter(is_active=True,).distinct()
    serializer_class = ActivitySerializer
