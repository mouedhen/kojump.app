# -*- coding: utf-8 -*-
import django_filters
from rest_framework import serializers

from equipments.models.activity import Activity


# class ActivityFilter(django_filters.FilterSet):
#
#     class Meta:
#         model = Activity
#         fields = ['activity__reference', ]


class ActivitySerializer(serializers.ModelSerializer):

    url = serializers.HyperlinkedIdentityField(view_name="api:activity-detail")

    class Meta:
        model = Activity
        fields = ('url', 'reference', 'label', 'image', 'is_active',)

    # filter_class = SportDisciplineFilter
