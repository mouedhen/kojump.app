# -*- coding: utf-8 -*-
import django_filters
from rest_framework import serializers

from equipments.models.discipline_sportive import SportDiscipline
from api.activities.serializer import ActivitySerializer


class SportDisciplineFilter(django_filters.FilterSet):

    class Meta:
        model = SportDiscipline
        fields = ['activity__reference', ]


class SportDisciplineSerializer(serializers.ModelSerializer):

    url = serializers.HyperlinkedIdentityField(view_name="api:sportdiscipline-detail")
    activity = ActivitySerializer(read_only=True)

    class Meta:
        model = SportDiscipline
        fields = ('url', 'reference', 'label', 'is_active', 'activity')

    filter_class = SportDisciplineFilter
