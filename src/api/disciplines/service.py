# -*- coding: utf-8 -*-
from rest_framework import viewsets

from .serializer import SportDisciplineSerializer
from equipments.models.discipline_sportive import SportDiscipline


class SportDisciplineViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = SportDiscipline.objects.all().filter(is_active=True,).distinct()
    serializer_class = SportDisciplineSerializer
