# -*- coding: utf-8 -*-
from rest_framework import viewsets
from django_filters import rest_framework as filters
from rest_framework_gis.pagination import GeoJsonPagination

from equipments.models.equipment import Equipment

from .serializer import EquipmentSerializer


class EquipmentFilter(filters.FilterSet):

    institution = filters.CharFilter(name='institution__code')

    class Meta:
        model = Equipment
        fields = []


class EquipmentViewSet(viewsets.ReadOnlyModelViewSet):

    pagination_class = GeoJsonPagination

    queryset = Equipment.objects.filter(is_active=True, disciplines__is_active=True,).distinct()
    serializer_class = EquipmentSerializer

    filter_class = EquipmentFilter
    filter_backends = (filters.DjangoFilterBackend,)
