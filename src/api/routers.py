# -*- coding: utf-8 -*-
from rest_framework import routers

from api.institutions.service import InstitutionViewSet
from api.disciplines.service import SportDisciplineViewSet
from api.activities.service import ActivityViewSet
from api.equipments.service import EquipmentViewSet

router = routers.DefaultRouter()

router.register(r'institutions', InstitutionViewSet)
router.register(r'disciplines', SportDisciplineViewSet)
router.register(r'activities', ActivityViewSet)
router.register(r'equipments', EquipmentViewSet)
