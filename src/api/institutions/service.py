# -*- coding: utf-8 -*-
from rest_framework import viewsets
from django_filters import rest_framework as filters

from rest_framework_gis.filters import DistanceToPointFilter
from rest_framework_gis.pagination import GeoJsonPagination

from equipments.models.institution import Institution
from equipments.models.activity import Activity
from equipments.models.discipline_sportive import SportDiscipline

from .serializer import InstitutionSerializer


def disciplines(request):

    if request is None:
        return Institution.objects.filter(is_active=True,).distinct()

    d = None
    request_filters = dict(request.GET)
    if 'disciplines' in request_filters:
        d = request_filters['disciplines']

    if not d:
        return SportDiscipline.objects.filter(is_active=True).distinct()
    return SportDiscipline.objects.filter(is_active=True, reference__in=d).distinct()


def activities(request):

    if request is None:
        return Institution.objects.filter(is_active=True,).distinct()

    a = None
    request_filters = dict(request.GET)
    if 'activities' in request_filters:
        a = request_filters['activities']

    if not a:
        return Activity.objects.filter(is_active=True).distinct()
    return Activity.objects.filter(is_active=True, reference__in=a).distinct()


class InstitutionFilter(filters.FilterSet):

    activity = filters.CharFilter(name='equipments__disciplines__activity_id')
    activities = filters.ModelMultipleChoiceFilter(name='equipments__disciplines__activity', queryset=activities)

    discipline = filters.CharFilter(name='equipments__disciplines__reference')
    disciplines = filters.ModelMultipleChoiceFilter(name='equipments__disciplines', queryset=disciplines,)

    department = filters.CharFilter(name='commune__department__code')
    commune = filters.CharFilter(name='commune__code_insee')

    hand_m = filters.BooleanFilter(name='is_accessible_for_hand_m')
    hand_s = filters.BooleanFilter(name='is_accessible_for_hand_s')

    class Meta:
        model = Institution
        fields = []


class InstitutionViewSet(viewsets.ReadOnlyModelViewSet):

    pagination_class = GeoJsonPagination
    # model = Institution
    queryset = Institution.objects.filter(is_active=True).distinct()
    # queryset = Institution.objects.filter(is_active=True, equipments__is_public=True).distinct()
    serializer_class = InstitutionSerializer

    distance_filter_field = 'equipments__gps_coordinates'

    filter_class = InstitutionFilter
    filter_backends = (DistanceToPointFilter, filters.DjangoFilterBackend)
