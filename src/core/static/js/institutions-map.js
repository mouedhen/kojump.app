(function () {

    /**
     * @todo handle algolia places input
     * @todo add more filters
     */

    var urlBuilder = new UrlBuilder('institutions');
    if (document.getElementById('activityId') !== null) {
        urlBuilder.buildUrlFromGetParams({activityInputId: 'activityId'});
    } else {
        urlBuilder.buildUrlFromGetParams({});
    }

    console.log(urlBuilder.url);

    document.getElementById('lng-search').value = urlBuilder.lng;
    document.getElementById('lat-search').value = urlBuilder.lat;
    document.getElementById('distance-search').value = urlBuilder.distance / 1000;

    function defineElChecked(elementId) {
        if (document.getElementById(elementId) !== null)
            document.getElementById(elementId).checked = true
    }

    if (urlBuilder.disciplines instanceof Array) {
        urlBuilder.disciplines.forEach(function (discipline) {
            defineElChecked('discipline_' + discipline)
        })
    } else {
        if (urlBuilder.disciplines !== undefined) {
            defineElChecked('discipline_' + urlBuilder.disciplines)
        }
    }

    if (urlBuilder.activities instanceof Array) {
        urlBuilder.activities.forEach(function (activity) {
            defineElChecked('activity_' + activity)
        })
    } else {
        if (urlBuilder.activities !== undefined) {
            defineElChecked('activity_' + urlBuilder.activities)
        }
    }

    L.mapbox.accessToken = 'pk.eyJ1IjoibW91ZWRoZW4iLCJhIjoiY2o1b25ibHdvMDFrbTJxcXRtaWttZ3VqcCJ9.D5l-yqWthlUroyn5GFoY2w';

    var map = L.mapbox.map('map', 'mapbox.streets')
        .setView([urlBuilder.lat, urlBuilder.lng], 13);

    L.control.locate({keepCurrentZoomLevel: true, flyTo: true}).addTo(map);

    map.addControl(L.mapbox.geocoderControl('mapbox.places', {autocomplete: true}));

    var featureLayer = L.mapbox.featureLayer()
        .loadURL(urlBuilder.url)
        .addTo(map);

    var circle = L.circle(new L.LatLng(urlBuilder.lat, urlBuilder.lng), urlBuilder.distance).addTo(map);

    var marker = L.marker(new L.LatLng(urlBuilder.lat, urlBuilder.lng), {
        icon: L.mapbox.marker.icon({
            'marker-color': '#3bb2d0',
            'marker-size': 'large',
            'marker-symbol': 'embassy'
        }),
        draggable: true
    })
        .addTo(map);

    map.on('locationfound', function (e) {
        marker.setLatLng(e.latlng);
        circle.setLatLng(e.latlng).setRadius(urlBuilder.distance);
        urlBuilder.lng = e.latlng.lng;
        urlBuilder.lat = e.latlng.lat;
        document.getElementById('lng-search').value = urlBuilder.lng;
        document.getElementById('lat-search').value = urlBuilder.lat;
        urlBuilder.buildUrl();
        featureLayer.loadURL(urlBuilder.url);
    });

    featureLayer.on('ready', function (e) {
        this.eachLayer(function (marker) {
            marker.setIcon(L.mapbox.marker.icon({
                'marker-size': 'small',
                'marker-symbol': 'star',
                // 'marker-color': (new FlatColor()).hex
                'marker-color': '#E65100'
            }));
            marker.bindPopup('<div><a href="/institutions/' + marker.feature.id + '/' + marker.feature.slug + '/">' +
                marker.feature.properties.label + '</a>\n' + '<p>' + marker.feature.properties.address + '</p>\n' +
                '</div>\n');
        });
        onmove();
        document.getElementById('progress-bar').style.display = 'none';
    });

    marker.on('dragstart', function () {
        document.getElementById('progress-bar').style.display = 'block';
        circle.setRadius(1);
    });

    marker.on('dragend', function (e) {
        circle.setLatLng(e.target.getLatLng()).setRadius(urlBuilder.distance);
        urlBuilder.lng = e.target.getLatLng().lng;
        urlBuilder.lat = e.target.getLatLng().lat;
        document.getElementById('lng-search').value = urlBuilder.lng;
        document.getElementById('lat-search').value = urlBuilder.lat;
        urlBuilder.buildUrl();
        featureLayer.loadURL(urlBuilder.url);
    });

    var $slider = $('.range-slider');
    var $range = $('.range-slider__range');

    $slider.each(function () {

        $('.range-slider__value').each(function () {
            $(this).html(urlBuilder.distance / 1000);
        });

        $range.on('input', function () {
            document.getElementById('progress-bar').style.display = 'block';
            urlBuilder.distance = this.value * 1000;
            circle.setRadius(urlBuilder.distance);
            urlBuilder.buildUrl();
            featureLayer.loadURL(urlBuilder.url)
        });
    });

    map.on('moveend', onmove);

    var renderFeature = function (feature) {
        var item = document.createElement('div');
        item.classList.add('activity');
        item.innerHTML = '<div class="uk-card uk-card-default uk-card-hover">\n' +
            '               <div class="uk-card-media-top">\n' +
            '                   <a href="/institutions/' + feature.id + '/' + feature.properties.slug + '/"><img src="' + feature.properties.image + '" alt="' + feature.properties.slug + '"></a>\n' +
            '               </div>\n' +
            '               <div class="uk-card-body">\n' +
            '                   <h3 class="uk-card-title">' +
            '                     <a href="/institutions/' + feature.id + '/' + feature.properties.slug + '/">' + feature.properties.label + '</a>' +
            '                   </h3>\n' +
            '                   <p>' + feature.properties.address + '</p>\n' +
            '               </div>\n' +
            '           </div>';
        return item;
    };

    function onmove() {
        var bounds = map.getBounds();
        document.getElementById('map-elements').innerHTML = '';
        featureLayer.eachLayer(function (marker) {
            if (bounds.contains(marker.getLatLng())) {
                document.getElementById('map-elements').appendChild(renderFeature(marker.feature))
            }
        });
    }
})();
