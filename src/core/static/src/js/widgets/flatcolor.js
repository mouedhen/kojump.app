/**
 * @class FlatColor
 * @description Generate a random sets of flat color with RGB and Hex values available for the user.
 * Sets can be random or based on a specific hue value.
 * @licence MIT
 * @author Mariam Maarouf <mrf.mariam@gmail.com>
 */
class FlatColor
{

    /**
     * The FlatColor class constructor.
     * The color is generated on class initiation.
     * @param h specific hue value
     */
    constructor (h = undefined) {
        this.hue = h;
        this._generateColor();
    }

    _generateColor () {
        let PHI = 0.618033988749895;
        if (this.hue === undefined) {
            let hue = (Math.floor(Math.random() * (360 + 1)))/360;
            this.hue = (hue + (hue / PHI)) % 360;
        } else {
            this.hue /= 360;
        }

        this.value = Math.floor(Math.random() * (100 - 20 + 1) + 20);
        this.saturation = (this.value - 10) / 100;
        this.value = this.value / 100;

        let i, f, p, q, t;
        i = Math.floor(this.hue * 6);
        f = this.hue * 6 - i;
        p = this.value * (1 - this.saturation);
        q = this.value * (1 - f * this.saturation);
        t = this.value * (1 - (1 - f) * this.saturation);

        switch (i % 6) {
            case 0:
                this.red = this.value;
                this.green = t;
                this.blue = p;
                break;
            case 1:
                this.red = q;
                this.green = this.value;
                this.blue = t;
                break;
            case 2:
                this.red = p;
                this.green = this.value;
                this.blue = t;
                break;
            case 3:
                this.red = p;
                this.green = q;
                this.blue = this.value;
                break;
            case 4:
                this.red = t;
                this.green = p;
                this.blue = this.value;
                break;
            case 5:
                this.red = this.value;
                this.green = p;
                this.blue = q;
                break;
        }
        this.red = Math.round(this.red * 255);
        this.green = Math.round(this.green * 255);
        this.blue = Math.round(this.blue * 255);

        this.hex = "#" + ((1 << 24) + (this.red << 16) + (this.green << 8) + this.blue).toString(16).slice(1);
    }

}

export default FlatColor;
