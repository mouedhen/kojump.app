import Rating from 'rating'

export class RatingStars {

    constructor (options = {containerEl: null, starEl: null, starsEl: null}, callback) {
        if (options.containerEl !== null && options.starEl !== null && options.starsEl !== null) {
            options.starEl.parentNode.removeChild(options.starEl);
            this.stars = options.starsEl;

            this.rating = new Rating([1, 2, 3, 4, 5], {
                container: options.containerEl,
                star: options.starEl
            });
            let that = this;
            this.rating.on('rate', function (weight) {
                if (weight !== undefined) {
                    that.rateHandler(weight);
                    console.log(weight);
                }
            });
            this.rating.on('select', function (weight) {
                if (weight !== undefined) {
                    that.rateHandler(weight);
                }
            });
        }
    }

    /*
    constructor(containerEl, starEl, starsEl) {
        if (containerEl !== null && starEl !== null && starsEl !== null) {

            starEl.parentNode.removeChild(starEl);
            this.stars = starsEl;

            import('rating').then((Rating) => {
                this.rating = new Rating([1, 2, 3, 4, 5], {
                    container: containerEl,
                    star: starEl
                });
                let that = this;
                this.rating.on('rate', function (weight) {
                    if (weight !== undefined) {
                        that.rateHandler(weight);
                        console.log(weight);
                    }
                });
                this.rating.on('select', function (weight) {
                    if (weight !== undefined) {
                        that.rateHandler(weight);
                    }
                });
            })
        }
    }
    */

    rateHandler(weight) {
        for (let i=0; i < this.stars.length; i++) {
            if (i < weight) {
                this.stars[i].firstChild.firstChild.nextElementSibling.setAttribute('fill', '#ffeb3b !important')
            } else {
                this.stars[i].firstChild.firstChild.nextElementSibling.setAttribute('fill', 'none')
            }
        }
    }
}

// export default RatingStars;