Contact Name:
{{ name }}

Email:
{{ email }}

Subject:
{{ subject }}

message:
{{ message|safe|striptags }}
