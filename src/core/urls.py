# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView
from core.views import ContactFormView


app_name = 'core'

urlpatterns = [
    url(r'^about/$',
        cache_page(60 * 15)(TemplateView.as_view(template_name='site/about.html')),
        name='about'),

    url(r'^contact-us/$',
        cache_page(60 * 15)(ContactFormView.as_view()),
        name='contact'),
]
