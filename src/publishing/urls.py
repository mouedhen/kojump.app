# -*- coding: utf-8 -*-
from django.conf.urls import url

from .views.favorite import json_set_favorite
from .views.comment import CommentOwnerListView


app_name = 'publishing'

urlpatterns = [
    url(r'likes/(?P<content_type_id>[^/]+)/(?P<object_id>[^/]+)/$', json_set_favorite, name='json_set_favorite'),
    url(r'comments/$', CommentOwnerListView.as_view(), name='comments-list'),
]
