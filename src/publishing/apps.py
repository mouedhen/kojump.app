# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.apps import AppConfig


class PublishingConfig(AppConfig):
    name = 'publishing'
    verbose_name = _(u'publication')
    verbose_name_plural = _(u'publications')
