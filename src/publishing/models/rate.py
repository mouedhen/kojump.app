# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey

from publishing.models.postable import Postable


@python_2_unicode_compatible
class Rate(Postable):

    RATING_CHOICES = (
        (1, _(u'très mauvais')),
        (2, _(u'mauvais')),
        (3, _(u'moyen')),
        (4, _(u'bon')),
        (5, _(u'très bon')),
    )

    class Meta:
        verbose_name = _(u'note')
        verbose_name_plural = _(u'notes')

    rating = models.IntegerField(_(u'note'), choices=RATING_CHOICES)

    # Generic relation to allow dynamic relation definition with comment
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    def __str__(self):
        return self.get_rating_display()
