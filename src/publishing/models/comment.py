# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey

from publishing.models.postable import Postable


@python_2_unicode_compatible
class Comment(Postable):

    STATUS = (
        ('NO', _(u'Non modéré')),
        ('YES', _(u'modéré')),
    )

    class Meta:
        verbose_name = _(u'commentaire')
        verbose_name_plural = _(u'commentaires')

    comment = models.TextField(_(u'commentaire'))
    status = models.CharField(_(u'statut'), max_length=3, choices=STATUS, default='NO')

    # Generic relation to allow dynamic relation definition with comment
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    def __str__(self):
        return self.comment
