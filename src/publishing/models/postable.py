# -*- coding: utf-8 -*-
from django.db import models
from django.core.exceptions import PermissionDenied

from django.contrib.auth.models import User

from utils.models.abstracts import TimeStampedAbstractModel, ActivatedAbstractModel


class Postable(TimeStampedAbstractModel, ActivatedAbstractModel):

    class Meta:
        abstract = True

    publisher = models.ForeignKey(User, null=True, blank=True)

    def set_publisher(self, user):
        if not user:
            raise PermissionDenied
        self.created_by = user

    def get_published(self, user):
        return self.objects.get(publisher=user)
