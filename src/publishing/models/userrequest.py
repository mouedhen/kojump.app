# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey

from publishing.models.postable import Postable


@python_2_unicode_compatible
class UserRequest(Postable):

    STATUS_CHOICE = (
        ('WAITING', _(u'en attente')),
        ('SUCCESS', _(u'accéder à la demande')),
        ('FAILURE', _(u'refuser la demande')),
    )

    SUBJECT_CATEGORIES = (
        ('PROPERTY', _(u'changement de propriétaire')),
        ('CLAIM', _(u'réclamation')),
        ('OTHERS', _(u'autres')),
    )

    REQUEST_OBJECT = (
        ('INSTITUTION', _(u'institution sportive')),
    )

    class Meta:
        verbose_name = _(u'requête utilisateur')
        verbose_name_plural = _(u'requêtes utilisateurs')

    subject = models.CharField(_('sujet'), max_length=100)
    content = models.TextField(_('contenu'))
    status = models.CharField(_(u'statut'), max_length=7, choices=STATUS_CHOICE, default='WAITING')
    category = models.CharField(_(u'catégorie'), max_length=8, choices=SUBJECT_CATEGORIES, default='PROPERTY')
    obj = models.CharField(_(u'objet'), max_length=14, choices=REQUEST_OBJECT, default='INSTITUTION')

    # Generic relation to allow dynamic relation definition with comment
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    def __str__(self):
        return self.subject
