# -*- coding: utf-8 -*-
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView

from publishing.models.comment import Comment


class CommentOwnerListView(LoginRequiredMixin, ListView):

    model = Comment
    template_name = 'publishing/comments-list.html'

    def get_queryset(self):
        return Comment.objects.filter(publisher=self.request.user).distinct()
