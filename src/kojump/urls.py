from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView

import legal.urls as legal_urls
import core.urls as core_urls
import accounts.urls as accounts_urls
import equipments.urls as equipments_urls
import publishing.urls as publishing_urls

from api.routers import router as api_router

from equipments.views.activity import ActivityListView


urlpatterns = [
    url(r'^$', ActivityListView.as_view(template_name='site/home.html'), name='home'),
    url(r'^(?P<page>\d+)$', ActivityListView.as_view(template_name='site/home.html'), name='home'),

    url(r'^', include(equipments_urls, namespace='equipments')),

    url(r'^activities/$', ActivityListView.as_view(), name='activities'),
    url(r'^activities/(?P<page>\d+)$', ActivityListView.as_view(), name='activities'),

    url(r'^', include(core_urls, namespace='core')),
    url(r'^legal/', include(legal_urls, namespace='legal')),

    url(r'^accounts/', include(accounts_urls, namespace='accounts')),
    url(r'^publishing/', include(publishing_urls, namespace='publishing')),

    url(r'^api/', include(api_router.urls, namespace='api')),

    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^admin/', admin.site.urls),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns