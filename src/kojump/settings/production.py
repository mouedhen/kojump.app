# -*- coding: utf-8 -*-
from .base import *

DEBUG = False

ALLOWED_HOSTS = [u'kojump.pythonanywhere.com', u'127.0.0.1', u'www.kojump.fr']

SPATIALITE_LIBRARY_PATH = "/home/kojump/spatialite/lib/libspatialite.so"

# CACHES = {
#     'default': {
#         'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
#         'LOCATION': '127.0.0.1:11211',
#     }
# }

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.org'
EMAIL_PORT = 587
EMAIL_HOST_USER = "webmaster.kojump@gmail.com"
EMAIL_HOST_PASSWORD = "3QZGW6cnV2Sx"
EMAIL_USE_TLS = True

DEFAULT_FROM_EMAIL = 'webmaster.kojump@gmail.com'
DEFAULT_RECIPIENT_LIST = ['webmaster.kojump@gmail.com']

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SECURE_SSL_REDIRECT = True
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True
