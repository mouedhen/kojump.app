# -*- coding: utf-8 -*-
import os
import logging

from django.core.management.base import BaseCommand
from django.conf import settings

from equipments.models.equipment_target import EquipmentTarget
from seeder.management.helpers.base import SluggedModelImporter

logger = logging.getLogger(__name__)


class Command(BaseCommand):

    help = 'Polpulate equipments targets datatable.'

    def handle(self, *args, **options):
        logging.debug('[SEEDER] start populating equipments targets datatable')
        families_importer = SluggedModelImporter(
            filename=os.path.join(settings.BASE_DIR, '../..', 'data', '9.equipments_targets.csv'),
            model=EquipmentTarget
        )
        families_importer.populate()
        logger.info('[SEEDER][POPULATING] equipments tagets populating end')
