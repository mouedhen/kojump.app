# -*- coding: utf-8 -*-
import os
import logging

from slugify import slugify

from django.core.management.base import BaseCommand
from django.conf import settings

from equipments.models.discipline_sportive import SportDiscipline
from equipments.models.activity import Activity
from seeder.management.helpers.core import ABSCoreCommandImporter

logger = logging.getLogger(__name__)


class ActivitiesImporter(ABSCoreCommandImporter):

    def serialize(self, row):
        reference = row['reference']
        label = row['label']
        slug = slugify(row['label'])
        activity_ref = row['activity_reference']
        if int(activity_ref) > 89:
            activity_ref = str(89)
        activity = Activity.objects.get(reference=activity_ref)
        is_active = activity.is_active
        logger.info('[SUCCESS][SERIALIZE] Record serialized - slug: {}'.format(slug))
        return self.model(reference=reference, label=label, slug=slug, is_active=is_active, activity=activity)


class Command(BaseCommand):

    help = 'Polpulate sports disciplines datatable.'

    def handle(self, *args, **options):
        logging.debug('[SEEDER] start populating sports disciplines datatable')
        families_importer = ActivitiesImporter(
            filename=os.path.join(settings.BASE_DIR, '../..', 'data', '4.sports_disciplines.csv'),
            model=SportDiscipline
        )
        families_importer.populate()
        logger.info('[SEEDER][POPULATING] sports disciplines populating end')
