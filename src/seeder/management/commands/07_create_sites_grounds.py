# -*- coding: utf-8 -*-
import os
import logging

from django.core.management.base import BaseCommand
from django.conf import settings

from equipments.models.site_ground import SiteGround
from seeder.management.helpers.base import SluggedModelImporter

logger = logging.getLogger(__name__)


class Command(BaseCommand):

    help = 'Polpulate equipments sites grounds datatable.'

    def handle(self, *args, **options):
        logging.debug('[SEEDER] start populating sites grounds datatable')
        families_importer = SluggedModelImporter(
            filename=os.path.join(settings.BASE_DIR, '../..', 'data', '7.sites_grounds.csv'),
            model=SiteGround
        )
        families_importer.populate()
        logger.info('[SEEDER][POPULATING] sites grounds populating end')
