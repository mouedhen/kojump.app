# -*- coding: utf-8 -*-
import os
import logging
from slugify import slugify

from django.core.management.base import BaseCommand
from django.conf import settings

from equipments.models.commune import Commune
from seeder.management.helpers.core import ABSCoreCommandImporter

logger = logging.getLogger(__name__)


class CommuneImporter(ABSCoreCommandImporter):

    def serialize(self, row):
        code_insee = row['code_insee']
        label = row['label']
        slug = slugify(label)
        department_id = row['department']
        logger.info('[SUCCESS][SERIALIZE] Record serialized - slug: {}'.format(slug))
        return self.model(code_insee=code_insee, label=label, slug=slug, department_id=department_id)


class Command(BaseCommand):

    help = 'Polpulate departments datatable.'

    def handle(self, *args, **options):
        logging.debug('[SEEDER] start populating departments datatable')
        families_importer = CommuneImporter(
            filename=os.path.join(settings.BASE_DIR, '../..', 'data', '13.communes.csv'),
            model=Commune
        )
        families_importer.populate()
        logger.info('[SEEDER][POPULATING] departments end')
