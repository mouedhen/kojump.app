# -*- coding: utf-8 -*-
import os
import logging

from django.core.management.base import BaseCommand
from django.conf import settings

from equipments.models.equipment_category_family import EquipmentCategoryFamily
from seeder.management.helpers.base import BaseImporter

logger = logging.getLogger(__name__)


class Command(BaseCommand):

    help = 'Polpulate equipments categories families datatable.'

    def handle(self, *args, **options):
        logging.debug('[SEEDER] start populating categories families datatable')
        families_importer = BaseImporter(
            filename=os.path.join(settings.BASE_DIR, '../..', 'data', '1.categories_families.csv'),
            model=EquipmentCategoryFamily
        )
        families_importer.populate()
        logger.info('[SEEDER][POPULATING] families end')
