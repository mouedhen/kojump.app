# -*- coding: utf-8 -*-
import os
import logging

from slugify import slugify

from django.core.management.base import BaseCommand
from django.conf import settings

from equipments.models.activity import Activity, generate_color
from seeder.management.helpers.core import ABSCoreCommandImporter

logger = logging.getLogger(__name__)


class ActivitiesImporter(ABSCoreCommandImporter):

    def serialize(self, row):
        activity_id = row['reference']
        label = row['label']
        slug = slugify(row['label'])
        pin_color = generate_color()
        is_active = row['is_active']
        logger.info('[SUCCESS][SERIALIZE] Record serialized - slug: {}'.format(slug))
        return self.model(pk=activity_id, label=label, slug=slug, pin_color=pin_color, is_active=is_active)


class Command(BaseCommand):

    help = 'Polpulate activities datatable.'

    def handle(self, *args, **options):
        logging.debug('[SEEDER] start populating activities datatable')
        families_importer = ActivitiesImporter(
            filename=os.path.join(settings.BASE_DIR, '../..', 'data', '3.activities.csv'),
            model=Activity
        )
        families_importer.populate()
        logger.info('[SEEDER][POPULATING] activities populating end')
