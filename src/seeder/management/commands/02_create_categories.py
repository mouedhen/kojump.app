# -*- coding: utf-8 -*-
import os
import logging
from slugify import slugify

from django.core.management.base import BaseCommand
from django.conf import settings

from equipments.models.equipment_category import EquipmentCategory
from seeder.management.helpers.core import ABSCoreCommandImporter

logger = logging.getLogger(__name__)


class EquipmentCategoryImporter(ABSCoreCommandImporter):

    def serialize(self, row):
        record_id = row['reference']
        label = row['label']
        slug = slugify(label)
        family_id = row['family_reference']
        logger.info('[SUCCESS][SERIALIZE] Record created')
        return self.model(id=record_id, label=label, slug=slug, family_id=family_id)


class Command(BaseCommand):

    help = 'Polpulate equipments categories datatable.'

    def handle(self, *args, **options):
        logging.debug('[SEEDER] start populating equipments categories datatable')
        families_importer = EquipmentCategoryImporter(
            filename=os.path.join(settings.BASE_DIR, '../..', 'data', '2.categories.csv'),
            model=EquipmentCategory
        )
        families_importer.populate()
        logger.info('[SEEDER][POPULATING] equipments categories end')
