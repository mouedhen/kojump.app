# -*- coding: utf-8 -*-
import os
import logging

from slugify import slugify

from django.core.management.base import BaseCommand
from django.conf import settings

from equipments.models.manager_category import ManagerCategory
from seeder.management.helpers.core import ABSCoreCommandImporter

logger = logging.getLogger(__name__)


class EquipmentCategoryImporter(ABSCoreCommandImporter):

    def serialize(self, row):
        label = row['label']
        slug = slugify(label)
        if int(row['is_public']) == 0:
            is_public = 'NOT_PUBLIC'
        elif int(row['is_public']) == 1:
            is_public = 'IS_PUBLIC'
        else:
            is_public = 'NOT_DEFINED'
        logger.info('[SUCCESS][SERIALIZE] Record serialized - slug: {}'.format(slug))
        return self.model(label=label, slug=slug, is_public=is_public)


class Command(BaseCommand):

    help = 'Polpulate managers categories datatable.'

    def handle(self, *args, **options):
        logging.debug('[SEEDER] start populating managers categories datatable')
        families_importer = EquipmentCategoryImporter(
            filename=os.path.join(settings.BASE_DIR, '../..', 'data', '10.managers_categories.csv'),
            model=ManagerCategory
        )
        families_importer.populate()
        logger.info('[SEEDER][POPULATING] managers categories populating end')
