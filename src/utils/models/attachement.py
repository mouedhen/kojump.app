# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey


@python_2_unicode_compatible
class AttachedMedia(models.Model):

    class Meta:
        verbose_name = _(u'media associé')
        verbose_name_plural = _(u'medias associés')

    image = models.ImageField(_('image'), upload_to='attached_media')

    # Generic relation to allow dynamic relation definition with comment
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()
