# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from slugify import slugify


class SluggedAbstractModel(models.Model):
    label = models.CharField(_('label'), max_length=150)
    slug = models.SlugField(_('slug'), max_length=150)

    def generate_slug(self):
        self.slug = slugify(self.label)

    class Meta:
        abstract = True


class DescribedAbstractModel(models.Model):
    description = models.TextField(_('description'), blank=True, null=True)

    class Meta:
        abstract = True


class TimeStampedAbstractModel(models.Model):
    created = models.DateTimeField(_('date de création'), auto_now_add=True)
    modified = models.DateTimeField(_('date de modification'), auto_now=True)

    class Meta:
        abstract = True


@python_2_unicode_compatible
class ActivatedAbstractModel(models.Model):
    is_active = models.BooleanField(_(u'est activé ?'), default=True)

    class Meta:
        abstract = True


@python_2_unicode_compatible
class LocatedAbstractModel(models.Model):
    street_address = models.CharField(_(u'adresse postale'), max_length=254, blank=True)
    street_address2 = models.CharField(_(u'adresse postale (complément)'), max_length=254, blank=True)
    postal_code = models.CharField(_(u'code postal'), max_length=10, blank=True)
    city = models.CharField(_(u'ville'), max_length=100, blank=True)
    country = models.CharField(_(u'pays'), max_length=100, blank=True)

    class Meta:
        abstract = True
