# -*- coding: utf-8 -*-
from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from utils.models.attachement import AttachedMedia


admin.site.register(AttachedMedia)
