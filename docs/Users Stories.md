# Users Stories

## Module utilisateurs

- [x] En tant qu'utilisateur, je veux pouvoir me connecter / me déconnecter à mon compte

    - [x] Créer la vue de connexion
    - [x] Créer le mockup de la page de connexion
    - [x] Ajouter les urls de connexion / de déconnexion

- [x] En tant qu'utilisateur, je veux pouvoir réinitialiser mon mot de passe en cas d'oublie

    - [x] Ajout des paramètres de configuration du service mailing
    - [x] Créer le mockup de l'email à envoyer en cas d'oublie de mot de passe
    - [x] Créer la vue de demande de réinitialisation du mot de passe
    - [x] Créer la vue de réinitialisation du mot de passe
    - [x] Créer la page de demande réinitialisation du mot de passe
    - [x] Créer la page de réinitialisation du mot de passe
    - [x] Créer la page de confirmation de réinitialisation du mot de passe
    - [x] Ajouter les urls relatives à la réinitialisation du mot de pasee

- [x] En tant qu'utilisateur, je veux pouvoir m'inscrire
    
    - [x] Ajouter un modèle profil pour l'enregistrement des informations relatives à l'utilisateur
    - [x] Créer les signaux de création automatique d'un profil lors de la création d'un utilisateur
    - [x] Créer le formulaire d'inscription
    - [x] Créer la vue d'inscription
    - [x] Créer la page d'inscription
    - [x] Ajouter les urls d'inscription

- [x] En tant qu'utilisateur, je veux pouvoir visualiser mon profil

    - [x] Créer la vue profil utilisateur
    - [x] Créer la page profil utilisateur
    - [x] Ajouter les urls pour la consultation du profil

- [x] En tant qu'utilisateur, je veux pouvoir modifier mon profil

    - [x] Créer le formulaire de modification du profil utilisateur
    - [x] Créer la vue de modification du profil utilisateur
    - [x] Ajouter les urls pour la modification du profil utilisateur

- [x] En tant qu'utilisateur, je veux pouvoir modifier mon mot de passe

    - [x] Créer la vue pour modifier le mot de passe
    - [x] Créer la page pour modifier le mot de passe
    - [x] Ajouter les urls pour la modification du mot de passe

- [x] En tant qu'administrateur, je veux pouvoir gérer les utilisateurs enregistrés

    - [x] Créer la classe d'administration pour le profil utilisateur
    - [x] Modifier la classe de base pour la gestion des utilisateurs et y attacher la classe de gestion des profil
    - [x] Défénir les filtres pour la vue liste
    - [x] Ajouter la possibilité d'importer et d'exporter les utilisateurs dans un fichier
    - [x] Créer une commande afin de créer un superutilisateur par défaut

## Module équipement
